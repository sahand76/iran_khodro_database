﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace iran_khodro.pages
{
    /// <summary>
    /// Interaction logic for Manager.xaml
    /// </summary>
    public partial class Manager : UserControl
    {
        private DataTable datatable = new DataTable();
        public Manager()
        {
            InitializeComponent();
            string connetionString;
            connetionString = @"Data Source=DESKTOP-UGU0NL9\SQLDEV;Initial Catalog=car_factory;Integrated Security=SSPI";
            string porseh = "select model,options,price,category,color,firstname,lastname from car left outer join customer on car.customer_id = customer.customer_id;";

            SqlConnection conn = new SqlConnection(connetionString);
            SqlCommand cmd = new SqlCommand(porseh, conn);
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
           
            datatable.Clear();
            da.Fill(datatable);
            iran_cars.ItemsSource = datatable.DefaultView;
            conn.Close();
            da.Dispose();
           
        }
        
        private void PopupBox_OnOpened(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Just making sure the popup has opened.");
        }

        private void PopupBox_OnClosed(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Just making sure the popup has closed.");
        }
        public void pull()
        {

            string date = spick.SelectedDate.Value.ToString("yyyy/MM/dd");
            string date2 = fpick.SelectedDate.Value.ToString("yyyy/MM/dd");
            string connetionString;
            connetionString = @"Data Source=DESKTOP-UGU0NL9\SQLDEV;Initial Catalog=car_factory;Integrated Security=SSPI";
            string porseh = "select model,options,price,category,color,firstname,lastname from car left outer join customer on car.customer_id = customer.customer_id where @s_date < s_date and @f_date> f_date;";
            
            SqlConnection conn = new SqlConnection(connetionString);
            SqlCommand cmd = new SqlCommand(porseh, conn);
            cmd.Parameters.Add("@s_date", date);
            cmd.Parameters.Add("@f_date", date2);
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            iran_cars.ItemsSource = null;
            datatable.Clear();
            da.Fill(datatable);
            iran_cars.ItemsSource = datatable.DefaultView;
            conn.Close();
            da.Dispose();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            pull();
        }

        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            Ccar c = new Ccar();
            Switcher.Switch(c);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Home h = new Home();
            Switcher.Switch(h);
        }
    }
}

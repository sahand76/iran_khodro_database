﻿using iran_khodro.pages;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace iran_khodro.Pages
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Customer : UserControl

    {
        private DataTable datatable = new DataTable();
        public Customer()
        {
            InitializeComponent();
            pull();
            
        }
        public void pull()
        {
            string connetionString;
            connetionString = @"Data Source=DESKTOP-UGU0NL9\SQLDEV;Initial Catalog=car_factory;Integrated Security=SSPI";
            string porseh = "select  model as Car_Model,options as Level_Options, category,color ,price,count(car_id) as Capacity from car where B_online != 1 and customer_id IS NULL group by model,options,price,category,color;";
            SqlConnection conn = new SqlConnection(connetionString);
            SqlCommand cmd = new SqlCommand(porseh, conn);
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(datatable);
            showcars.ItemsSource = datatable.DefaultView;
            conn.Close();
            da.Dispose();
        }

        private void Showcar_Click(object sender, RoutedEventArgs e)
        {

            string date = null;
            if (pick.SelectedDate.HasValue)
            {
                date = pick.SelectedDate.Value.ToString("yyyy/MM/dd");
                Console.WriteLine(date);
            }
            string connetionString;
            connetionString = @"Data Source=DESKTOP-UGU0NL9\SQLDEV;Initial Catalog=car_factory;Integrated Security=SSPI";
            string porseh = "select  model as Car_Model,options as Level_Options,category,color,price,count(car_id) as Capacity from car where @today > s_date and @today<f_date and B_online!=1 and customer_id IS NULL group by model,options,color,category,price; ";
            SqlConnection conn = new SqlConnection(connetionString);
            SqlCommand cmd = new SqlCommand(porseh, conn);
            cmd.Parameters.Add("@today", date);
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable datatable2 = new DataTable();
            da.Fill(datatable2);
            showcars.ItemsSource = datatable2.DefaultView;
            conn.Close();
            da.Dispose();


        }
        
        public void btnView_Click(object sender, RoutedEventArgs e)
        {
            DataRowView dataRowView = (DataRowView)((Button)e.Source).DataContext;
            Globals.smodel = dataRowView[0].ToString();
            Globals.soptions= dataRowView[1].ToString();
            Globals.scategory = dataRowView[2].ToString();
            Globals.scolor = dataRowView[3].ToString();
            Globals.sprice = dataRowView[4].ToString();
            Console.WriteLine(Globals.smodel);
            Buy b = new Buy();
            Switcher.Switch(b);
        }

        private void Showcar_Click2(object sender, RoutedEventArgs e)
        {
            Home h = new Home();
            Switcher.Switch(h);
        }
    }
    static class Globals
    {
        public static string smodel;
        public static string soptions;
        public static string scategory;
        public static string scolor;
        public static string sprice;
        
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace iran_khodro.pages
{
    /// <summary>
    /// Interaction logic for Ccar.xaml
    /// </summary>
    public partial class Ccar :UserControl
    {
        public Ccar()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string date = spick.SelectedDate.Value.ToString("yyyy/MM/dd");
            string date2 = fpick.SelectedDate.Value.ToString("yyyy/MM/dd");
            string date3=dpick.SelectedDate.Value.ToString("yyyy/MM/dd");
            string model = imodel.Text;
            string option = ioption.Text;
            string category = icat.Text;
            string color = icolor.Text;
            string branch_id = ibranch.Text;
            string price = iprice.Text;
            string connetionString;
            connetionString = @"Data Source=DESKTOP-UGU0NL9\SQLDEV;Initial Catalog=car_factory;Integrated Security=SSPI";
            string porseh = "select max(car_id)+1 from car";
            SqlConnection conn = new SqlConnection(connetionString);
            SqlCommand cmd = new SqlCommand(porseh, conn);

            conn.Open();
            int car_id = (int)cmd.ExecuteScalar();
            conn.Close();
            string pors2 = "insert into car(car_id,model,options,Branch_id,price,category,color,due,s_date,f_date) values(@car_id, @model, @options, @Branch_id, @price, @category, @color, @due, @s_date, @f_date); ";
            SqlCommand cmd2 = new SqlCommand(pors2, conn);
            cmd2.Parameters.Add("@car_id", car_id);
            cmd2.Parameters.Add("@branch_id", branch_id);
            cmd2.Parameters.Add("@model", model);
            cmd2.Parameters.Add("@options", option);
            cmd2.Parameters.Add("@category", category);
            cmd2.Parameters.Add("@color", color);
            cmd2.Parameters.Add("@price", price);
            cmd2.Parameters.Add("@s_date", date);
            cmd2.Parameters.Add("@f_date", date2);
            cmd2.Parameters.Add("@due", date3);
            conn.Open();
            cmd2.ExecuteNonQuery();
            conn.Close();
            Manager m = new Manager();
            Switcher.Switch(m);


        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Manager m = new Manager();
            Switcher.Switch(m);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace iran_khodro.pages
{
    /// <summary>
    /// Interaction logic for B_car.xaml
    /// </summary>
    public partial class B_car : UserControl
    {
        private DataTable datatable = new DataTable();
        public B_car()
        {
            InitializeComponent();
        }
        public void pull()
        {
            string branchid = b_id.Text;
            string connetionString;
            connetionString = @"Data Source=DESKTOP-UGU0NL9\SQLDEV;Initial Catalog=car_factory;Integrated Security=SSPI";
            string porseh = "select model,options,price,category,car_id,color,firstname,lastname from car join customer on car.customer_id = customer.customer_id where car.Branch_id = @branch and tahvil=0; ";
            SqlConnection conn = new SqlConnection(connetionString);
            SqlCommand cmd = new SqlCommand(porseh, conn);
            cmd.Parameters.Add("@branch", branchid);
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(datatable);
            branch_cars.ItemsSource = datatable.DefaultView;
            conn.Close();
            da.Dispose();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            pull();

        }
        public void btnView_Click(object sender, RoutedEventArgs e)
        {
            DataRowView dataRowView = (DataRowView)((Button)e.Source).DataContext;
            string karid = dataRowView[4].ToString();
            string connetionString;
            connetionString = @"Data Source=DESKTOP-UGU0NL9\SQLDEV;Initial Catalog=car_factory;Integrated Security=SSPI";
            string pro = "select email  from customer join car on car.customer_id = customer.customer_id where car_id = @car_id; ";
            SqlConnection conni = new SqlConnection(connetionString);
            SqlCommand cmdd = new SqlCommand(pro, conni);
            cmdd.Parameters.Add("@car_id", karid);
            conni.Open();
            object maili = cmdd.ExecuteScalar();
            string mymail = maili.ToString();
            conni.Close();
            
            
           
            string bepors = "update car set tahvil = 1 where car_id = @car_id; ";
            SqlConnection conn = new SqlConnection(connetionString);
            SqlCommand cmd = new SqlCommand(bepors, conn);
            cmd.Parameters.Add("@car_id", karid);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            B_car b = new B_car();
            Switcher.Switch(b);

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Home h = new Home();
            Switcher.Switch(h);
        }
    }
}

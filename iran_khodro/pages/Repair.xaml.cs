﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace iran_khodro.pages
{
    /// <summary>
    /// Interaction logic for Repair.xaml
    /// </summary>
    public partial class Repair : UserControl
    {
        private DataTable datatable = new DataTable();
        int temp;
        
        public Repair()
        {
            
            InitializeComponent();
        }
        private void PopupBox_OnOpened(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Just making sure the popup has opened.");
        }

        private void PopupBox_OnClosed(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Just making sure the popup has closed.");
        }
        public void pull()
        {
            string porseh=null;
            string date = null;
            string date2 = null;
            string branchid = b_id.Text;
            string connetionString;
            connetionString = @"Data Source=DESKTOP-UGU0NL9\SQLDEV;Initial Catalog=car_factory;Integrated Security=SSPI";

            SqlConnection conn = new SqlConnection(connetionString);
            SqlCommand cmd = new SqlCommand(porseh, conn);
            if (spick.SelectedDate.HasValue && fpick.SelectedDate.HasValue)
            {
                date = spick.SelectedDate.Value.ToString("yyyy/MM/dd");
                date2 = fpick.SelectedDate.Value.ToString("yyyy/MM/dd");
                porseh = "select * from Repair_Car where Branch_id = @branch_id and tarikh> @s_date and tarikh<@f_date;";
                cmd = new SqlCommand(porseh, conn);
                cmd.Parameters.Add("@s_date", date);
                cmd.Parameters.Add("@f_date", date2);
                cmd.Parameters.Add("@branch_id", branchid);
               
                
            }
            else
            {
                porseh = "select * from Repair_Car where Branch_id = @br;";
                cmd = new SqlCommand(porseh, conn);
                cmd.Parameters.Add("@br", branchid);
                
            }
           
            
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            branch_repair.ItemsSource = null;
            datatable.Clear() ;
            da.Fill(datatable);
            branch_repair.ItemsSource = datatable.DefaultView;
            conn.Close();
            da.Dispose();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string branch = b_id.Text;
            object gg;
            pull();
            int custo_id;
            string connetionString;
            connetionString = @"Data Source=DESKTOP-UGU0NL9\SQLDEV;Initial Catalog=car_factory;Integrated Security=SSPI";
            string porseh = "select sum(total) from (select sum(car.price) * 0.02 as total from Repair_Car join car on Repair_Car.car_id = car.car_id where DATEDIFF(month, due, convert(date, SYSDATETIME())) < 3 and car.Branch_id = @branch_id group by car.car_id) as tmp; ";
            SqlConnection conn = new SqlConnection(connetionString);
            SqlCommand cmd = new SqlCommand(porseh, conn);
            cmd.Parameters.Add("@branch_id", branch);
            conn.Open();
            gg=cmd.ExecuteScalar();
            Console.WriteLine(gg);
            conn.Close();
            var myMessageQueue = new SnackbarMessageQueue(TimeSpan.FromMilliseconds(8000));
            myMessageQueue.Enqueue("Irankhodro Ows you " +gg.ToString()+ "Toman");
            SnackbarFive.MessageQueue = myMessageQueue;
        }
       

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            crepair temp = new crepair();
            Switcher.Switch(temp);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            B_car b = new B_car();
            Switcher.Switch(b);
        }
    }
}

﻿using iran_khodro.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace iran_khodro.pages
{

    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    /// 
    public partial class Home : UserControl
    {
        
        public Home()
        {

            InitializeComponent();
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Manager m = new Manager();
            Switcher.Switch(m);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Customer c = new Customer();
            Switcher.Switch(c);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            B_car b = new B_car();
            Switcher.Switch(b);
        }
    }
}

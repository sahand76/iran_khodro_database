﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace iran_khodro.pages
{
    /// <summary>
    /// Interaction logic for crepair.xaml
    /// </summary>
    public partial class crepair : UserControl
    {
        public crepair()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string rclass = r_class.Text;
            string carid = car_id.Text;
            string cid = customer_id.Text;
            string rprice = price.Text;
            string date = null;
            date = pick.SelectedDate.Value.ToString("yyyy/MM/dd");
            string connetionString;
            connetionString = @"Data Source=DESKTOP-UGU0NL9\SQLDEV;Initial Catalog=car_factory;Integrated Security=SSPI";
            string porseh = "INSERT INTO Repair_Car(tarikh,Repair_price,Repair_class,Branch_id,car_id,customer_id) VALUES(@tarikh, @repair_price, @repair_class, @branch_id, @car_id, @customer_id); ";
            SqlConnection conn = new SqlConnection(connetionString);
            SqlCommand cmd = new SqlCommand(porseh, conn);
            cmd.Parameters.Add("@tarikh", date);
            cmd.Parameters.Add("@repair_price", rprice);
            cmd.Parameters.Add("@repair_class", rclass);
            cmd.Parameters.Add("@branch_id", "501");
            cmd.Parameters.Add("@car_id", carid);
            cmd.Parameters.Add("@customer_id", cid);
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
            Repair r = new Repair();
            Switcher.Switch(r);

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Repair r = new Repair();
            Switcher.Switch(r);
        }
    }
}

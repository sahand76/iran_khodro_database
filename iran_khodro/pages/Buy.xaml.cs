﻿using iran_khodro.Pages;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace iran_khodro.pages
{
    /// <summary>
    /// Interaction logic for buy.xaml
    /// </summary>
    public partial class Buy : UserControl
    {
        string customer_id;
        public Buy()
        {
            InitializeComponent();
        }

        public void Button_Click(object sender, RoutedEventArgs e)
        {
            string connetionString;
            connetionString = @"Data Source=DESKTOP-UGU0NL9\SQLDEV;Initial Catalog=car_factory;Integrated Security=SSPI";
            string pmodel = iran_khodro.Pages.Globals.smodel;
            string poptions = iran_khodro.Pages.Globals.soptions;
            string pcategory = iran_khodro.Pages.Globals.scategory;
            string pcolor = iran_khodro.Pages.Globals.scolor;
            string pprice = iran_khodro.Pages.Globals.sprice;

            int ccid;
            string por = "select top 1 car_id from car where model = @model and options = @options and category = @category and color = @color and price = @price and B_online != 1; ";
            SqlConnection co = new SqlConnection(connetionString);
            SqlCommand cm = new SqlCommand(por, co);
            cm.Parameters.Add("@model", pmodel);
            cm.Parameters.Add("@options", poptions);
            cm.Parameters.Add("@category", pcategory);
            cm.Parameters.Add("@color", pcolor);
            cm.Parameters.Add("@price", pprice);

            co.Open();
            ccid = (int)cm.ExecuteScalar();
            G2.carr_id = ccid.ToString();
            co.Close();
            Console.WriteLine(ccid);

            int custo_id;

            if (string.IsNullOrEmpty(c_id.Text))
            {
                string first = cname.Text;
                string last = clast.Text;
                string city = ccity.Text;
                string aven = caven.Text;
                string plate = cplate.Text;
                string gender = cgen.Text;
                string mail = cmail.Text;
                string phone = cphone.Text;
                string gid = cgid.Text;
                string cid;


                string porseh = "select max(customer_id)+1 from customer";
                SqlConnection conn = new SqlConnection(connetionString);
                SqlCommand cmd = new SqlCommand(porseh, conn);

                conn.Open();
                custo_id = (int)cmd.ExecuteScalar();
                G2.cccid = custo_id.ToString();
                conn.Close();
                string porse2 = "INSERT INTO customer(customer_id,firstname,lastname,city,avenue,plate,gender,email,phone,Geo_id) VALUES(@temp, @firstname, @lastname, @city, @avenue, @plate, @gender, @email, @phone, @Geo_io);update car set customer_id = @temp where car_id = @car_id; ";

                SqlCommand cmd2 = new SqlCommand(porse2, conn);
                cmd2.Parameters.Add("@temp", custo_id);
                cmd2.Parameters.Add("@firstname", first);
                cmd2.Parameters.Add("@lastname", last);
                cmd2.Parameters.Add("@city", city);
                cmd2.Parameters.Add("@avenue", aven);
                cmd2.Parameters.Add("@plate", plate);
                cmd2.Parameters.Add("@gender", gender);
                cmd2.Parameters.Add("@email", mail);
                cmd2.Parameters.Add("@phone", phone);
                cmd2.Parameters.Add("@Geo_io", gid);
                cmd2.Parameters.Add("@car_id", ccid);
                conn.Open();
                cmd2.ExecuteNonQuery();


                conn.Close();

            }
            else
            {
                customer_id = c_id.Text;
                G2.cccid = customer_id;
                string porseh = "update car set customer_id = @customer_id where car_id = @car_id; ";
                SqlConnection conn = new SqlConnection(connetionString);
                SqlCommand cmd = new SqlCommand(porseh, conn);
                cmd.Parameters.Add("@customer_id", customer_id);
                cmd.Parameters.Add("@car_id", ccid);
                conn.Open();
                cmd.ExecuteNonQuery();


                conn.Close();

            }
            RenderTargetBitmap bitmap = new RenderTargetBitmap((int)this.ss.ActualWidth, (int)this.ss.ActualHeight, 96, 96, PixelFormats.Pbgra32);
            bitmap.Render(this.ss);
            Final f = new Final();
            Switcher.Switch(f);
            using (FileStream stream = File.Create(@"C:\Users\msi\Desktop\ax.jpeg"))
            {
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.QualityLevel = 90;
                encoder.Frames.Add(BitmapFrame.Create(bitmap));
                encoder.Save(stream);
            }
        }

        private void Showcar_Click2(object sender, RoutedEventArgs e)
        {
            Customer h = new Customer();
            Switcher.Switch(h);
        }

    }
   
    
}
static class G2
{
    public static string cccid;
    public static string carr_id;
   
    


}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace iran_khodro.pages
{
    /// <summary>
    /// Interaction logic for Final.xaml
    /// </summary>
    public partial class Final : UserControl
    {
        public Final()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string cid = G2.cccid;
            string connetionString;
            connetionString = @"Data Source=DESKTOP-UGU0NL9\SQLDEV;Initial Catalog=car_factory;Integrated Security=SSPI";

            Home h = new Home();
            Switcher.Switch(h);
            string porseh= "declare @branch int; declare @cgeoid int;declare @bcity varchar(255);declare @ccity varchar(255);declare @id int;BEGIN DECLARE db_cursor CURSOR FOR select branch.Geo_id,customer.Geo_id,branch.City,customer.city,branch.Branch_id from[dbo].[customer] cross join[dbo].[branch] where customer.customer_id=@cid OPEN db_cursor FETCH NEXT FROM db_cursor into @branch, @cgeoid, @bcity, @ccity, @id WHILE @@FETCH_STATUS = 0 BEGIN if(@branch= @cgeoid and @bcity = @ccity) begin select branch.Branch_id from branch cross join customer where branch.Geo_id=@branch and branch.City= @ccity and customer_id = @cid BREAK end FETCH NEXT FROM db_cursor into  @branch,@cgeoid,@bcity,@ccity,@id END; CLOSE db_cursor DEALLOCATE db_cursor; END;";
            SqlConnection conn = new SqlConnection(connetionString);
            SqlCommand cmd = new SqlCommand(porseh, conn);
            cmd.Parameters.Add("@cid", cid);
            conn.Open();
            object obj = cmd.ExecuteScalar();
            string ans1 = "";
            if (obj != null)
            {
                 ans1 = obj.ToString();
            }
            
            conn.Close();
            if (string.IsNullOrEmpty(ans1))
            {
                string porseh2 = "select top 1 branch.Branch_id from branch, customer where customer_id = @cid";
                SqlCommand cmd2 = new SqlCommand(porseh2, conn);
                cmd2.Parameters.Add("@cid", cid);
                conn.Open();
                object obj2 = cmd2.ExecuteScalar();
                ans1 = obj2.ToString();
                conn.Close();
            }
            try
            {
                string pro = "select email  from customer  where customer_id=@cid; ";
                SqlCommand cmo = new SqlCommand(pro, conn);
                cmo.Parameters.Add("@cid", cid);
                conn.Open();
                object maili = cmo.ExecuteScalar();
                string mymail = maili.ToString();
                conn.Close();
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress("sahandabaspour76@gmail.com");
                mail.To.Add(mymail);
                mail.Subject = "Car purchase";
                mail.Body = "Hello Dear Customer with Customer Id as " + cid + " you have bought a Car " + "\n" + "Soon your car will be ready\n " + " nearest IranKhodro Branch to you is: \n The Branch with ID: " +ans1 ;
                System.Net.Mail.Attachment attachment;
                attachment = new System.Net.Mail.Attachment("C:/Users/msi/Desktop/ax.jpeg");
                mail.Attachments.Add(attachment);
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("sahandabaspour76", "09378918824");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                MessageBox.Show("mail Send");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            string porsm = "update car set Branch_id = @bid where car.car_id = @carid";
            SqlCommand cmo2 = new SqlCommand(porsm, conn);
            cmo2.Parameters.Add("@bid", ans1);
            cmo2.Parameters.Add("@carid", G2.carr_id);
            conn.Open();
            cmo2.ExecuteNonQuery();

            conn.Close();

           


        }
    }
}
